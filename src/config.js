import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyAPcO5y7Lo7-jtk7fszdv_igMNaz1LKzUc",
    authDomain: "forbbits-6ebe3.firebaseapp.com",
    databaseURL: "https://forbbits-6ebe3.firebaseio.com",
    projectId: "forbbits-6ebe3",
    storageBucket: "forbbits-6ebe3.appspot.com",
    messagingSenderId: "136324591849",
    appId: "1:136324591849:web:2ef9db25c63c5f45688078",
    measurementId: "G-5C14RCBVXH"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export const db = firebase.database()