import React from 'react';
import { StyleSheet, View, Dimensions, StatusBar } from 'react-native';
import Index from './page/App';

const statusBarHeight = StatusBar.currentHeight;

export default function App() {
  return (
    <Index />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: statusBarHeight,
  },
});