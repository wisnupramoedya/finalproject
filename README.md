# FinalProject
Proyek ini bernama FinalPro. Berisi daftar anggota mahasiswa ormada. Namun, belum memiliki CRUD yang baik/register-nya masih belum berfungsi.

## Cara Penggunaan
1. Ketika ingin memilih Register, seluruh input harus terisi. (tapi fitur save register belum bisa)
2. Lantas bisa menekan ke halaman Login, bisa coba mengakses API di bawah untuk melihat email dan password setiap user
3. Setelah itu, akan mendapat tampilan Home berisi seluruh anggota dan nama akun
4. Bisa memilih klik icon akun untuk menuju ke halaman Profile (namun fitur save belum berfungsi, karena sifatnya CRUD)
5. Bisa juga memilih klik anggota untuk ke halaman Detail dari setiap anggota

## Penggunaan
Penggunaan API dari buatan sendiri: [API Forbbits](https://forbbits-6ebe3.firebaseio.com/)

## Developer
Anggota yang mengikuti proyek ini:
1. Pramudya Tiandana Wisnu Gautama (wisnupramoedya@gmail.com)
2. Thomas Felix Brilliant (felixbrilliant@yahoo.co.id)

Terima kasih kepada mentor mas Achmad Hilmy