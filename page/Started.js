import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';

export default class StartedScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image source={require('./../assets/logo.png')} style={{width: 165, height: 158, marginTop: 121}} />
                <Text style={styles.textTitle}>Forbbits</Text>
                <TouchableOpacity style={styles.buttonStarted} onPress={() => this.props.navigation.navigate('Register')}>
                    <Text style={{fontSize: 20, fontFamily: 'Roboto', color: 'white', fontWeight: 'bold'}}>GET STARTED</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.buttonLogin} onPress={() => this.props.navigation.navigate('Login')}>
                    <Text style={{fontSize: 20, fontFamily: 'Roboto', color: '#0065B0', fontWeight: 'bold'}}>LOGIN</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        fontFamily: 'Calibri',
        backgroundColor: '#F8F8F8'
    },

    textTitle: {
        color: '#0065B0',
        fontSize: 36,
        fontWeight: 'bold',
        marginTop: 28
    },

    buttonStarted: {
        backgroundColor: '#0065B0',
        width: 336,
        height: 52,
        borderRadius: 24,
        alignItems: 'center',
        marginTop: 128,
        justifyContent: 'center'
    },

    buttonLogin: {
        backgroundColor: 'white',
        width: 336,
        height: 52,
        borderRadius: 24,
        alignItems: 'center',
        borderColor: '#003366',
        borderWidth: 1,
        marginTop: 21,
        justifyContent: 'center'
    }
});