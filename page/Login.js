import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TextInput,
    Alert
} from 'react-native';
import { db } from '../src/config'
import { useDispatch } from 'react-redux';
import { AsyncStorage } from 'react-native';

export default class LoginScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {},
            userName: '',
            password: '',
            isError: false,
            secureTextEntry: true,
            activate: false,
            database: []
        }
    }

    componentDidMount(){
        db.ref('/users').on('value', querySnapShot => {
            let data = querySnapShot.val() ? querySnapShot.val() : {};
            let database =[...data];
            this.setState({database})
        });
    }

    handleLogin() {
        console.log(this.state.userName, ' ', this.state.password);

        if (this.state.userName === "" || this.state.password === "") {
            Alert.alert(
                "Warning",
                "Field Tidak Boleh Kosong",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
        }
        // else if (this.state.userName === "" || this.state.password === "") {
        //     Alert.alert(
        //         "Warning",
        //         "Field Tidak Boleh Kosong",
        //         [
        //             { text: "OK", onPress: () => console.log("OK Pressed") }
        //         ],
        //         { cancelable: false }
        //     );
        // }
        else {
            let goHome = false

            this.state.database.forEach(user => {
                if(this.state.userName === user.email && this.state.password === user.pass){
                    goHome = true;
                    this.setState({user: user})
                }
            })
            if(goHome == false){
                Alert.alert(
                    "Warning",
                    "Email atau password salah",
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
                    );
                }
                else{
                    if(this.state.activate){
                        return this.props.navigation.navigate('Home' ,  {user: this.state.user});
                    }
                    else this.setState({activate: true})
            }
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.textTitle}>Sign In</Text>
                <View style={styles.boxForm}>
                    <TextInput
                        style={styles.textForm}
                        placeholder="Username / Email"
                        onChangeText={userName => this.setState({ userName })}
                        autoCapitalize="none"
                        underlineColorAndroid = 'rgba(0, 0, 0, 0)'
                    />
                </View>
                <View style={styles.boxForm}>
                    <TextInput
                        style={styles.textForm}
                        placeholder="Password"
                        secureTextEntry={true}
                        onChangeText={password => this.setState({ password })}
                        autoCapitalize="none"
                    />
                </View>
                <TouchableOpacity style={styles.buttonLogin} onPress={() => this.handleLogin()}>
                    <Text style={{fontSize: 20, fontFamily: 'Roboto', color: 'white', fontWeight: 'bold'}}>LOGIN</Text>
                </TouchableOpacity>
                <Text style={{marginTop: 43, fontSize: 15}}>You are completely safe.</Text>
                <Text style={{fontSize: 15, color: '#0065B0'}}>Read our Terms of Conditions</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        fontFamily: 'Calibri',
        backgroundColor: '#F8F8F8'
    },

    textTitle: {
        color: '#0065B0',
        fontSize: 36,
        fontWeight: 'bold',
        marginTop: 101
    },

    boxForm: {
        backgroundColor: 'white',
        width: 336,
        height: 52,
        borderRadius: 24,
        marginTop: 25,
        justifyContent: 'center'
    },

    textForm: {
        fontSize: 20,
        color: '#2C2929',
        fontWeight: 'bold',
        opacity: 0.4,
        marginLeft: 26
    },

    buttonLogin: {
        backgroundColor: '#0065B0',
        width: 336,
        height: 52,
        borderRadius: 24,
        alignItems: 'center',
        marginTop: 215,
        justifyContent: 'center'
    },
});