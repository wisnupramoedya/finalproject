import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TextInput,
    Alert
} from 'react-native';

export default class RegisterScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
          userName: '',
          password: '',
          email: '',
          confirmPassword: '',
          isError: false,
          secureTextEntry: true
        }
    }

    handleRegister() {
        if (this.state.userName === "" && this.state.password === "" && this.state.email === "" && this.state.confirmPassword === "") {
            Alert.alert(
                "Warning",
                "Field Tidak Boleh Kosong",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
        }
        else if (this.state.userName === "" || this.state.password === "" || this.state.email === "" || this.state.confirmPassword === "") {
            Alert.alert(
                "Warning",
                "Field Tidak Boleh Kosong",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
        }

        else if (this.state.password !== this.state.confirmPassword) {
            Alert.alert(
                "Warning",
                "Password Tidak Sesuai",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
        }
        else {
            this.props.navigation.navigate('Login' ,  {userName: this.state.userName})
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.textTitle}>Sign Up</Text>
                <View style={styles.boxForm}>
                    <TextInput
                        style={styles.textForm}
                        placeholder="Username"
                        autoCapitalize="none"
                        underlineColorAndroid = 'rgba(0, 0, 0, 0)'
                        onChangeText={userName => this.setState({ userName })}
                    />
                </View>
                <View style={styles.boxForm}>
                    <TextInput
                        style={styles.textForm}
                        placeholder="Email"
                        keyboardType="email-address"
                        autoCapitalize="none"
                        underlineColorAndroid = 'rgba(0, 0, 0, 0)'
                        onChangeText={email => this.setState({ email })}
                    />
                </View>
                <View style={styles.boxForm}>
                    <TextInput
                        style={styles.textForm}
                        placeholder="Password"
                        secureTextEntry={true}
                        autoCapitalize="none"
                        underlineColorAndroid = 'rgba(0, 0, 0, 0)'
                        onChangeText={password => this.setState({ password })}
                    />
                </View>
                <View style={styles.boxForm}>
                    <TextInput
                        style={styles.textForm}
                        placeholder="Confirm Password"
                        secureTextEntry={true}
                        autoCapitalize="none"
                        underlineColorAndroid = 'rgba(0, 0, 0, 0)'
                        onChangeText={confirmPassword => this.setState({ confirmPassword })}
                    />
                </View>
                <TouchableOpacity style={styles.buttonLogin} onPress={() => this.handleRegister()}>
                    <Text style={{fontSize: 20, fontFamily: 'Roboto', color: 'white', fontWeight: 'bold'}}>REGISTER</Text>
                </TouchableOpacity>
                <Text style={{marginTop: 43, fontSize: 15}}>You are completely safe.</Text>
                <Text style={{fontSize: 15, color: '#0065B0'}}>Read our Terms of Conditions</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        fontFamily: 'Calibri',
        backgroundColor: '#F8F8F8'
    },

    textTitle: {
        color: '#0065B0',
        fontSize: 36,
        fontWeight: 'bold',
        marginTop: 101
    },

    boxForm: {
        backgroundColor: 'white',
        width: 336,
        height: 52,
        borderRadius: 24,
        marginTop: 25,
        justifyContent: 'center'
    },

    textForm: {
        fontSize: 20,
        color: '#2C2929',
        fontWeight: 'bold',
        opacity: 0.4,
        marginLeft: 26
    },

    buttonLogin: {
        backgroundColor: '#0065B0',
        width: 336,
        height: 52,
        borderRadius: 24,
        alignItems: 'center',
        marginTop: 215,
        justifyContent: 'center'
    },
});