import React, { Component } from 'react'
import { Text, StyleSheet, View, FlatList } from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons'
import { db } from '../src/config'
import { TouchableOpacity } from 'react-native-gesture-handler'

const Content = (props) => {
    return (
      <TouchableOpacity style={styles.content} onPress={props.onPress} >
        <View style={{flexDirection: 'row'}} >
            <View style={[styles.logoContent, {backgroundColor: '#EB5757'}]}>
                <Text style={[styles.font, {fontSize: 24, color: 'white'}]} >
                    TC
                </Text>
            </View>
            <View style={styles.nameContent}>
                <Text style={[styles.font, styles.fontContent, {color: '#EB5757'}]} >
                    {props.data.name}
                </Text>
                <Text style={[styles.font, styles.fontContent]} >
                    {props.data.major}
                </Text>
            </View>
        </View>
        <Text style={[styles.font, styles.fontContent, {color: '#2C2929', opacity: 0.4}]} >
            {props.data.year}
        </Text>
      </TouchableOpacity>
    )
}

export default class Home extends Component {
    constructor(){
        super();
        this.state = {
            database: []
        }
    }

    componentDidMount() {
        db.ref('/users').on('value', querySnapShot => {
            let data = querySnapShot.val() ? querySnapShot.val() : {};
            let database =[...data];
            this.setState({database})
        });
      }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header} >
                    <Text style={[styles.font, styles.title, {color: colors.black}]}>
                        Hai, <Text style={{color: colors.primary}}>{this.props.route.params.user.name}</Text>
                    </Text>
                    <FontAwesome5 name="user-alt" size={24} color="black" onPress={() => this.props.navigation.navigate('Profile', {user: this.props.route.params.user})} />
                </View>
                <Text style={[styles.font, styles.title, {color: colors.secondary, marginTop: 35}]} >
                    Kenalan Yuk
                </Text>
                <FlatList style={{marginTop: 10}}
                    data={this.state.database}
                    renderItem={({item}) => <Content data={item} onPress={() => this.props.navigation.navigate('Detail', {item})} /> }
                    keyExtractor={(item) => item.id}
                    ItemSeparatorComponent={() => <View style={{height: 22}} />}
                />
            </View>
        )
    }
}

const colors = {
    primary: '#0065B0',
    secondary: '#28A4FF',
    black: '#2C2929',
    white: '#FFFFFF',
    bg: '#F8F8F8'
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg,
        padding: 44
    },
    font: {
        fontFamily: 'Roboto',
        fontStyle: 'normal'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 28,
        lineHeight: 33
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    content: {
        borderColor: 'rgba(0, 0, 0, 0.2)',
        borderRadius: 10,
        borderWidth: 1,
        padding: 4,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    logoContent: {
        borderRadius: 20,
        height: 60,
        width: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },
    nameContent: {
        marginLeft: 10
    },
    fontContent: {
        fontSize: 18,
    }
})
