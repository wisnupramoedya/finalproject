import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import { Ionicons } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class Profile extends Component {
    render() {
        const {name, major, year, description} = this.props.route.params.user
        return (
            <View style={styles.container}>
                <View style={styles.title}>
                    <Ionicons name="ios-arrow-round-back" size={40} color="black" onPress={() => this.props.navigation.navigate('Home')} />
                    <Text style={[styles.fontTitle, {marginLeft: 20}]} >
                        Detail
                    </Text>
                </View>
                <View style={[styles.body, {marginTop: 18}]}>
                    <View style={{flexDirection: 'row'}}>
                        <Image source={require('../assets/profile.png')} style={{width: 54, height: 54, borderRadius: 20}} />
                        <View style={{marginLeft: 14}}>
                            <Text style={styles.fontName} >{name}</Text>
                            <Text style={[styles.fontDesc, styles.colorDesc]} >{major}</Text>
                        </View>
                    </View>
                    <View style={{marginTop: 26}} >
                        <Text style={styles.fontSubname}>Description</Text>
                        <Text style={[styles.fontDesc, styles.colorDesc]} >
                            {description}
                        </Text>
                    </View>
                    <View style={{marginTop: 26}} >
                        <Text style={styles.fontSubname}>About</Text>
                        <Text style={[styles.fontDesc]} >
                            Jurusan        : {major}
                        </Text>
                        <Text style={[styles.fontDesc]} >
                            Angkatan       : {year}
                        </Text>
                    </View>
                </View>
                <TouchableOpacity style={styles.buttonLogin}>
                    <Text style={{fontSize: 20, fontFamily: 'Roboto', color: 'white', fontWeight: 'bold'}}>SAVE</Text>
                </TouchableOpacity>
            </View>         
        )
    }
}

const colors = {
    primary: '#0065B0',
    secondary: '#28A4FF',
    black: '#2C2929',
    white: '#FFFFFF',
    bg: '#F8F8F8'
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg,
        padding: 44
    },
    title: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    font: {
        fontFamily: 'Roboto'
    },
    fontTitle: {
        fontSize: 34
    },
    fontName: {
        fontSize: 25,
    },
    fontSubname: {
        fontSize: 20
    },
    fontDesc: {
        fontSize: 16,
        marginTop: 13
    },
    colorDesc: {
        color: '#99879D'
    },
    buttonLogin: {
        backgroundColor: '#0065B0',
        width: 336,
        height: 52,
        borderRadius: 24,
        alignItems: 'center',
        marginTop: 200,
        justifyContent: 'center'
    },
})
