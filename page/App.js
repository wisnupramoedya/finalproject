import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import StartedScreen from './Started';
import LoginScreen from './Login';
import RegisterScreen from './Register';
import HomeScreen from './Home';
import ProfileScreen from './Profile';
import DetailScreen from './Detail'

export default class Index extends Component {
    render() {
        const Stack = createStackNavigator();
        return (
            <NavigationContainer>
                <Stack.Navigator initialRouteName="LoginScreen">
                    <Stack.Screen name="Started" component={StartedScreen} options={{headerShown: false}} />
                    <Stack.Screen name="Login" component={LoginScreen} options={{headerShown: false}} />
                    <Stack.Screen name="Register" component={RegisterScreen} options={{headerShown: false}} />
                    <Stack.Screen name="Home" component={HomeScreen} options={{headerShown: false}} />
                    <Stack.Screen name="Profile" component={ProfileScreen} options={{headerShown: false}} />
                    <Stack.Screen name="Detail" component={DetailScreen} options={{headerShown: false}} />
                </Stack.Navigator>
            </NavigationContainer>
        )
    }
}